# CLI Dose comparison

## gamma_pass.sh
Gamma pass dose comparison on the commandline using plastimatch.

The program requires `dcmtk` and `plastimatch`. Both can be installed in a debian system by running

`sudo apt install dcmtk plastimatch`

Tested on Kubuntu 20.04, but should work on any distribution (at least debian based I would guess).

Usage:
`./gamma_pass.sh <dicom_dir> --mindose 10 --outputfilename "my_gamma_passes.txt" --dose-dta 3 --dist-dta 3`

For gamma passes on the body, CTV, PTV, bladder and rectum on all voxels over 10% of the reference dose with a 3 mm / 3% dose criterium.

The program will find a structure file and two RTDOSE files. If it finds too many structure-files or dose files it will ask for specification.

The program will look for reference RTDOSE file with "plan", "rtdose", or "ref" in the name.

The program will look for a test RTDOSE file with "sim", "test", "fluka", "flk", "topas" or "geant" in the name.

Otherwise the program will just find 2 RTDOSE files.

For more information run 

`./gamma_pass.sh -h` or `./gamma_pass.sh --help`
