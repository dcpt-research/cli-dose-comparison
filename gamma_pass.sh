#!/bin/bash

function help()
{
    echo " ---------------------------- "
    echo " --- gamma pass evaluator ---"
    echo " ---------------------------- "
    echo " Calculates gamma pass between two RTDOSE files found in patient_directory"
    echo " in a structure found in an RTSTRUCT file in the same directory."
    echo ""
    echo " Usage: ./gamma_pass.sh patient_directory"
    echo " Arguments: "
    echo "    patient_directory:      Path to a directory containing at "
    echo "                            least 1 RTSTRUCT file and 2 RTDOSE files."
    echo " Options: "
    echo "   -h:                      print this help message"
    echo "   -m --mindose:            The minimum dose to consider in percent of the max dose. "
    echo "                            Default 10 %"
    echo "   -o --outputfilename:     The name of the output file. It will be placed in <patient_directory>."
    echo "                            Default name "qck_gamma_passes.txt""
    echo "  --dose-dta:               The dose agreement criteria in percent. Default 3 %."
    echo "  --dist-dta:               The distance agreement criteria in mm. Default 3 mm."
    echo ""
    echo " Requires following to be in PATH: "
    echo "  * Plastimatch"
    echo "  * dcmtk (more specifically dcmdump)"
    echo " See http://plastimatch.org/"
    echo "     https://support.dcmtk.org/docs/"
    echo " ---------------------------- "
    echo " You can name your DICOM files to help the program."
    echo " The program will look for a reference RTDOSE called something with"
    echo " >>> \"plan\", \"rtdose\" or \"ref\""
    echo " while looking for a test RTDOSE called something with"
    echo " >>> \"sim\", \"test\", \"fluka\", \"flk\", \"topas\", \"geant\""
    echo " The program will look for a RTSTRUCT called something with"
    echo " >>> \"struct\""
    echo " ---------------------------- "

}

dose_dta=3 # percent
dist_dta=3 # mm
mindose=10
output_file="qck_gamma_passes.txt"
POSITIONAL_ARGS=()
while [[ $# -gt 0 ]]; do
  case $1 in
    -h|--help)
      help
      shift # past argument
      exit 0
      ;;
    -m|--mindose)
      mindose="$2"
      shift # past argument
      shift # past value
      ;;
    -o|--outputfilename)
      output_file="$2"
      shift # past argument
      shift # past value
      ;;
    --dose-dta)
      dose_dta="$2"
      shift # past argument
      shift # past value
      ;;
    --dist-dta)
      dist_dta="$2"
      shift # past argument
      shift # past value
      ;;
    -*|--*)
      echo "Unknown option $1"
      exit 1
      ;;
    *)
      POSITIONAL_ARGS+=("$1") # save positional arg
      shift # past argument
      ;;
  esac
done
set -- "${POSITIONAL_ARGS[@]}"

pat_fold=${POSITIONAL_ARGS[0]}

# if no patient directory given print error and exit
if [[ "$pat_fold" == "" ]];
then
    echo " >>> No patient folder specified. Please specify a directory when calling the program."
    echo ""
    echo ""
    help
    exit 0
fi
# Check if plastimatch is in path
if [[ "$(which plastimatch)" == "" ]]; 
then
    echo " >>> You need to have plastimatch in your path."
    echo ""
    echo ""
    help
    exit 0
fi

str=""
dos=""
testdose=""

SAVEIFS=$IFS   # Save current IFS (Internal Field Separator)
IFS=$'\n'      # Change IFS to newline char
dump=$(dcmdump $(find ${pat_fold} -name "*.dcm" -exec printf ''%s'\n' {} \;))

strsopInUID=$(echo "$dump" | grep RTSTRUCT -B10 | grep SOPInstanceUID | cut -d "[" -f2 | cut -d "]" -f1)
dossopInUID=$(echo "$dump" | grep RTDOSE -B10 | grep SOPInstanceUID | cut -d "[" -f2 | cut -d "]" -f1)

### splitting dose UIDS into an array
dossopInUID=($dossopInUID) # split the `names` string into an array by the same name
IFS=$SAVEIFS   # Restore original IFS


if [[ "$strsopInUID" != "" ]];
then
    str=$(find ${pat_fold} -maxdepth 1 -name "*${strsopInUID}.dcm")
    ### if there is not a file with this SOP UID in it's name. 
    ### So now we loop through all files to find the RTSTRUCT. Will take longer of course.
    if [[ "$str" == "" ]]; 
    then
        ### search by keyword
	temp_file=$(mktemp)
	find ${pat_fold} -maxdepth 1 -iname "*struct*.dcm" -print0 | while IFS= read -r -d '' file;
		do
			t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
			if [[ "${t_dump}" == "RTSTRUCT" ]]
			then
				echo "$file" > $temp_file;
				echo "RTSTRUCT found by keyword."
				break
			fi
		done
	str="$(cat $temp_file)"
	if [[ "$str" == "" ]]; 
        then
            echo "RTSTRUCT file not found immediately. "
            echo "Finding it now by searching (might take longer)..."
			find ${pat_fold} -maxdepth 1 -iname "*.dcm" -print0 | while IFS= read -r -d '' file;
            do
                t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
                if [[ "${t_dump}" == "RTSTRUCT" ]]
                then
					echo "$file" > $temp_file;
                    echo "RTSTRUCT found by searching."
                    break
                fi
            done
			str="$(cat $temp_file)"
        fi
    else
        echo "Found RTSTRUCT by SOP UID matching."
    fi
    
fi

if [[ "$str" == "" ]];
then
    str=$(find ${pat_fold} -name "*${strsopInUID}.dcm")
    ### if there is not a file with this SOP UID in it's name. 
    ### So now we loop through all files to find the RTSTRUCT. Will take longer of course.
    if [[ "$str" == "" ]]; 
    then
        ### search by keyword
	find ${pat_fold} -iname "*struct*.dcm" -print0 | while IFS= read -r -d '' file;
        do
            t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
            if [[ "${t_dump}" == "RTSTRUCT" ]]
            then
                echo "$file" > $temp_file
                echo "RTSTRUCT found by keyword."
                break
            fi
        done
	str="$(cat $temp_file)"

	if [[ "$str" == "" ]]; 
        then
            echo "RTSTRUCT file not found immediately. "
            echo "Finding it now by searching (might take longer)..."
	    find ${pat_fold} -iname "*.dcm" -print0 | while IFS= read -r -d '' file;
            do
                t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
                if [[ "${t_dump}" == "RTSTRUCT" ]]
                then
					echo "$file" > $temp_file
                    echo "RTSTRUCT found by searching."
                    break
                fi
            done
		str="$(cat $temp_file)"
        fi
    fi
fi
if [[ "$str" == "" ]];
then
    echo "No RTSTRUCT file was found in given folder."
    echo ""
    echo ""
    help
    exit 0
fi

temp_file=$(mktemp)
# now looking for reference dose
if [[ "${dossopInUID[0]}" != "" && "${dossopInUID[1]}" != "" ]];
then
    ### if there is not a file with this SOP UID in it's name. 
    ### So now we loop through all files to find the RTSTRUCT. Will take longer of course.
    ### First look for the reference dose

    ### First a keyword search
	find ${pat_fold} -iname "*plan*.dcm" -o -iname "*ref*.dcm" -o iname "*rtdose*.dcm" -print0 | while IFS= read -r -d '' file;
		do
			t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
			if [[ "${t_dump}" == "RTDOSE" ]]
			then
				echo "$file" > $temp_file
				echo "Reference dose found it by keyword."
				break
			fi
		done
	dos="$(cat $temp_file)"
    if [[ "$dos" == "" ]]; 
    then
        dos=$(find ${pat_fold} -name "*${dossopInUID[0]}.dcm")
        if [[ "$dos" == "" ]];
        then
            echo "Reference RTDOSE file not found immediately. "
            echo "Finding it now by searching (might take longer)..."
			find ${pat_fold} -maxdepth 1 -iname "*.dcm" -print0 | while IFS= read -r -d '' file;
				do
					t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
					if [[ "${t_dump}" == "RTDOSE" ]]
					then
						echo "$file" > $temp_file
						echo "Reference dose found it by searching."
						break
					fi
				done
			dos="$(cat $temp_file)"
            if [[ "$dos" == "" ]];
            then
				find ${pat_fold} -iname "*.dcm" -print0 | while IFS= read -r -d '' file;
                do
                    t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
                    if [[ "${t_dump}" == "RTDOSE" ]]
                    then
						echo "$file" > $temp_file
                        echo "Reference dose found it by searching."
                        break
                    fi
                done
				dos="$(cat $temp_file)"
            fi

        else
          echo "Reference RTDOSE found by SOP UID matching."
        fi
    fi
else
    echo "No RTDOSE files in given folder."
    echo ""
    echo ""
    help
    exit 0
fi


### now looking for a test dose
### dont need to check for files in folder since we would have exited already
temp_file=$(mktemp)
find ${pat_fold} \( -iname "*sim*.dcm" \
                            -o -iname "*test*.dcm" \
                            -o -iname "*fluka*.dcm" \
                            -o -iname "*flk*.dcm" \
                            -o -iname "*topas*.dcm" \
                            -o -iname "*geant*.dcm" \) -print0 | while IFS= read -r -d '' file;
	do
		echo "$file"
		if [[ "$file" == *[b,B][i,I][o,O]* && "$file" != *[u,U][n,N][c,C][e,E][r,R][t,T]* ]];
		then
		echo "$file"
			t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
			if [[ "${t_dump}" == "RTDOSE" ]]
			then
				echo "$file" > $temp_file
				echo "Test dose found it by keyword."
			echo "$testdose"
				break
			fi
		echo "$testdose inside if statement in loop"
		fi
	done
testdose="$(cat $temp_file)"
echo "About to test if $testdose is null"
if [[ "$testdose" == "" ]];
then
	echo "$testdose is null"
	find ${pat_fold} \( -iname "*sim*.dcm" \
                            -o -iname "*test*.dcm" \
                            -o -iname "*fluka*.dcm" \
                            -o -iname "*flk*.dcm" \
                            -o -iname "*topas*.dcm" \
                            -o -iname "*geant*.dcm" \) -print0 | while IFS= read -r -d '' file;
		do
			t_dump=$(dcmdump "${file}" | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
			if [[ "${t_dump}" == "RTDOSE" ]]
			then
				echo "$file" > $temp_file
				echo "Test dose found it by keyword."
			echo "$testdose"
				break
			fi
		done
	testdose="$(cat $temp_file)"
fi

echo "about to test if $testdose is null"
if [[ "$testdose" == "" ]];
then
    echo "$testdose is null"
    testdose="$(find ${pat_fold} -name "*${dossopInUID[1]}.dcm")"
    if [[ "$testdose" == "$dos" ]];
    then
        testdose="$(find ${pat_fold} -name "*${dossopInUID[0]}.dcm")"
    fi
    if [[ "$testdose" == "" || "$testdose" == "$dos" ]];
    then
        echo "Test RTDOSE file not found immediately. "
        echo "Finding it now by searching (might take longer)..."
		find ${pat_fold} -name "*.dcm" -not -name "*${dos##*/}" -print0 | while IFS= read -r -d '' file;
			do
				t_dump=$(dcmdump ${file} | grep Modality | cut -d "[" -f2 | cut -d "]" -f1)
				if [[ "${t_dump}" == "RTDOSE" ]]
				then
					echo "$file" > $temp_file
					echo "Test dose found it by searching."
					break
				fi
			done
		testdose="$(cat $temp_file)"
    fi
fi

echo "Calculating gamma indices for:"
echo "Structure file: ${str##*/}"
echo "Reference dose: ${dos##*/}"
echo "Test dose     : ${testdose##*/}"


plastimatch convert --input $str --output-prefix ${pat_fold}/masks --prefix-format nrrd --output-ss-list ${pat_fold}/masks/image.txt --input-dose-img "$dos"



echo "Geometric gamma passes ${dist_dta} mm / ${dose_dta} % for all voxels over ${mindose} %" > ${pat_fold}/"${output_file}"

echo "Reference dose used: ${dos}"
echo "Test dose used     : ${testdose}"
dose_dta_f=$(echo "scale=2;${dose_dta}/100" | bc)
mindose_f=$(echo "scale=1;${mindose}/100" | bc)

find masks/ -iname "*rectum*" -exec printf ''%s'\n' {} \; 2> /dev/null | head -1

echo "Gamma: BODY = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} "${dos}" "${testdose}" --mask "$(find ${pat_fold}/masks -iname "*body*.nrrd" -exec printf ''%s'\n' {} \; 2> /dev/null | head -1)" --interp-search | tail -1 | cut -d "=" -f2)" >> ${pat_fold}/"${output_file}"
echo "Gamma: CTV = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} "${dos}" "${testdose}" --mask "$(find ${pat_fold}/masks -iname "*ctv*.nrrd" -exec printf ''%s'\n' {} \; 2> /dev/null | head -1)" --interp-search | tail -1 | cut -d "=" -f2)" >> ${pat_fold}/"${output_file}"
echo "Gamma: PTV = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} "${dos}" "${testdose}" --mask "$(find ${pat_fold}/masks -iname "*ptv*.nrrd" -exec printf ''%s'\n' {} \; 2> /dev/null | head -1)" --interp-search | tail -1 | cut -d "=" -f2)" >> ${pat_fold}/"${output_file}"
echo "Gamma: Bladder = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} "${dos}" "${testdose}" --mask "$(find ${pat_fold}/masks -iname "*bladder*.nrrd" -exec printf ''%s'\n' {} \; 2> /dev/null | head -1)" --interp-search | tail -1 | cut -d "=" -f2)" >> ${pat_fold}/"${output_file}"
echo "Gamma: Rectum = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} "${dos}" "${testdose}" --mask "$(find ${pat_fold}/masks -iname "*rectum*.nrrd" -exec printf ''%s'\n' {} \; 2> /dev/null | head -1)" --interp-search | tail -1 | cut -d "=" -f2)" >> ${pat_fold}/"${output_file}"

# ## ONLY FOR TESTING
# echo "TESTING NOW"
# echo "Gamma: Rectum = plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} "${dos}" "${testdose}" --mask $(find ${pat_fold}/masks -iname "*rectum*.nrrd" -exec printf ''%s'\n' {} \; 2> /dev/null | head -1) --interp-search"
# echo "plastimatch gamma"
# echo " --analysis-threshold ${mindose_f}" 
# echo "--dose-tolerance ${dose_dta_f}" 
# echo "--dta-tolerance ${dist_dta}" 
# echo "${dos}" 
# echo "${testdose}" 
# echo "--mask "${pat_fold}/masks/[b,B][o,O][d,D][y,Y].nrrd""
# echo " --interp-search" 
# # echo "Gamma: CTV = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} ${dos} ${testdose} --mask ${pat_fold}/masks/[c,C][t,T][v,V].nrrd --interp-search | tail -1 | cut -d "=" -f2)"
# # echo "Gamma: PTV = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} ${dos} ${testdose} --mask ${pat_fold}/masks/[p,P][t,T][v,V].nrrd --interp-search | tail -1 | cut -d "=" -f2)"
# # echo "Gamma: Bladder = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} ${dos} ${testdose} --mask ${pat_fold}/masks/[b,B][l,L][a,A][d,D][d,D][e,E][r,R].nrrd --interp-search | tail -1 | cut -d "=" -f2)"
# # echo "Gamma: Rectum = $(plastimatch gamma --analysis-threshold ${mindose_f} --dose-tolerance ${dose_dta_f} --dta-tolerance ${dist_dta} ${dos} ${testdose} --mask ${pat_fold}/masks/*[r,R][e,E][c,C][t,T][u,U][m,M]*.nrrd --interp-search | tail -1 | cut -d "=" -f2)"
# ## ONLY FOR TESTING



echo "Gamma pass analysis done!"


	
